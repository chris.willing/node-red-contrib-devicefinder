
module.exports = function(RED) {
  const { XKeys, XKeysWatcher } = require('xkeys');
  let watcher;
  let msg;
  let xkeys_devices;
  let xkeys_names;

  function DeviceFinder(config) {
    RED.nodes.createNode(this,config);
    var node = this;
    var flowContext = this.context().flow;

    // xkeys_devices is a dictionary of device objects,
    // keyed by their unique ids.
    xkeys_devices = flowContext.get("xkdevs") || {};
    flowContext.set("xkdevs", xkeys_devices);

    // xkeys_names is a dict of device names,
    // keyed by their devices unique ids.
    xkeys_names = flowContext.get("xknames") || {};
    flowContext.set("xknames", xkeys_names);

    node.log("DeviceFinder is starting a new watcher");
    msg = {};
    msg["payload"] = "newWatcher";
    node.send(msg);

    // Input messages
    node.on('input', function(msg) {
    
		if (msg.payload == "findDevices") {
			startWatcher();
		} else if (msg.payload == "restarted") {
			node.log("restarting");
			startWatcher();
		} else if (msg.payload == "sendInfo") {
			node.send({payload: "deviceInfo", info: xkeys_devices[msg.target].info});
		} else if (msg.payload == "identify") {
			xkeys_devices[msg.target].setFrequency(20);
			xkeys_devices[msg.target].setIndicatorLED(2, true, true)
			setTimeout((device) => {
				device.setIndicatorLED(2, false);
			}, 3000, xkeys_devices[msg.target]);
		} else if (msg.payload == "reboot") {
			//node.log("device reboot requested for " + msg.target);
			xkeys_devices[msg.target].rebootDevice();
		} else {
			node.send({payload: "empty"});
		}
	});

	function startWatcher() {
		// Don't reuse previous instance when flow is redeployed
		if (watcher) {
			node.log("Already have a watcher");
			watcher.stop();
		}
		watcher = new XKeysWatcher();

		watcher.on('connected', (xkeysPanel) => {
			//node.log(`X-keys panel ${xkeysPanel.uniqueId} connected`)
			xkeys_devices = flowContext.get("xkdevs");
			xkeys_devices[xkeysPanel.uniqueId] = xkeysPanel;
			flowContext.set("xkdevs", xkeys_devices);
			update_xkeys_names();

			var msg = {};
			msg["payload"] = "newDevice";
			msg["uniqueId"] = xkeysPanel.uniqueId;
			msg["device_list"] = flowContext.get("xknames");
			node.send(msg);

			xkeysPanel.on('disconnected', () => {
				//node.log(`X-keys panel of type ${xkeysPanel.info.name} disconnected`)
				xkeys_devices = flowContext.get("xkdevs");
				delete xkeys_devices[xkeysPanel.uniqueId];
				flowContext.set("xkdevs", xkeys_devices);
				msg = {};
				msg["payload"] = "lostDevice";
				msg["uniqueId"] = xkeysPanel.uniqueId;
				node.send(msg);

				// Clean up
				xkeysPanel.removeAllListeners();
				xkeysPanel.close();
			})

			xkeysPanel.on('down', (btnIndex, metadata) => {
				//node.log('Button pressed', btnIndex, metadata, xkeysPanel.uniqueId);
				msg = {};
				msg["payload"] = "deviceEvent";
				msg["data"] = [xkeysPanel.uniqueId,
				`button ${btnIndex} down, row ${metadata['row']}, col ${metadata['col']}, timestamp ${metadata['timestamp']}` ];
				node.send(msg);
			})
			xkeysPanel.on('up', (btnIndex, metadata) => {
				//node.log('Button released', btnIndex, metadata, xkeysPanel.uniqueId);
				msg = {};
				msg["payload"] = "deviceEvent";
				msg["data"] = [xkeysPanel.uniqueId,
				`button ${btnIndex} up, row ${metadata['row']}, col ${metadata['col']}, timestamp ${metadata['timestamp']}` ];
				node.send(msg);
			})
			xkeysPanel.on('jog', (index, deltaPos, metadata) => {
				//node.log('Jog ${index} position changed', deltaPos, metadata, xkeysPanel.uniqueId);
				msg = {};
				msg["payload"] = "deviceEvent";
				msg["data"] = [xkeysPanel.uniqueId,
				`jog ${index} delta ${deltaPos}, timestamp ${metadata['timestamp']}` ];
				node.send(msg);
			})
			xkeysPanel.on('shuttle', (index, shuttlePos, metadata) => {
				//node.log('Shuttle ${index} position changed', shuttlePos, metadata, xkeysPanel.uniqueId);
				msg = {};
				msg["payload"] = "deviceEvent";
				msg["data"] = [xkeysPanel.uniqueId,
				`shuttle ${index} pos ${shuttlePos}, timestamp ${metadata['timestamp']}` ];
				node.send(msg);
			})
			xkeysPanel.on('joystick', (index, position, metadata) => {
				//node.log('Joystick ${index} position changed', position, metadata, xkeysPanel.uniqueId); // {x,y,z}
				msg = {};
				msg["payload"] = "deviceEvent";
				msg["data"] = [xkeysPanel.uniqueId,
				`joystick ${index} xPos ${position['x']}, yPos ${position['y']}, twist ${position['deltaZ']}, timestamp ${metadata['timestamp']}` ];
				node.send(msg);
			})
			xkeysPanel.on('tbar', (index, position, metadata) => {
				//node.log('T-bar ${index} position changed', position, metadata, xkeysPanel.uniqueId);
				msg = {};
				msg["payload"] = "deviceEvent";
				msg["data"] = [xkeysPanel.uniqueId,
				`tbar ${index} pos ${position}, timestamp ${metadata['timestamp']}` ];
				node.send(msg);
			})
			xkeysPanel.on('reconnect', (data) => {
				node.log('RECONNECT event', data, xkeysPanel.uniqueId);
			})
			xkeysPanel.on('error', (...errs) => {
				node.log('X-keys watcher error:', ...errs);
			})
		});

	} // startWatcher

	function update_xkeys_names () {
		xkeys_names = {};
		var xkeys_devices = flowContext.get("xkdevs");
		for (const key of Object.keys(xkeys_devices)) {
			xkeys_names[key] = xkeys_devices[key].product.name;
		}
		flowContext.set("xknames", xkeys_names);
	} // update_xkeys_names


  }
  RED.nodes.registerType("devicefinder", DeviceFinder);

}
