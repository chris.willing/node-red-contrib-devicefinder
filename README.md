# node-red-contrib-devicefinder

node-red-contrib-devicefinder is a module for [Node-RED](https://nodered.org/) for interacting with USB connected [X-keys](https://xkeys.com/) devices using [SuperFlyTV's](https://github.com/SuperFlyTV/xkeys) [nodejs](https://nodejs.org) library.


## Dependencies

Apart from Node-RED itself, the node-red-contrib-devicefinder module depends on [xkeys](https://github.com/SuperFlyTV/xkeys) (2.2.1) and [usb-detection](https://github.com/MadLittleMods/node-usb-detection) (4.10.0).

The provided example flow additionally depends on [node-red-dashboard](https://github.com/node-red/node-red-dashboard) (2.30.0).


## Installation

For normal use in the Node-RED web interface, the _node-red-contrib-devicefinder_ node is most conveniently installed using the Palette Manager. In the _Manage palette_ widget, select the _Install_ tab and search for _devicefinder_ which should soon list _node-red-contrib-devicefinder_. Select the associated _install_ button to complete the installation. A new devicefinder node should now be available in the _function_ section the flow editor's palette side-bar.

If the installation initially fails with messages like:
```
 [err] ERR! usb-detection@4.10.0 install: `prebuild-install || node-gyp rebuild`
 [err] npm ERR! Exit status 1
 [err] npm ERR!
 [err] npm ERR!
 [err]  Failed at the usb-detection@4.10.0 install script.
```
then run the following commands in a terminal:
```
sudo apt install -y build-essential
sudo apt install libudev-dev
```
Now try again to install _node-red-contrib-devicefinder_ as above.


Of course the code itself may be also downloaded by [cloning the repo](https://gitlab.com/chris.willing/node-red-contrib-devicefinder.git) or by running `npm install node-red-contrib-devicefinder`


## Usage

An example flow to demonstrate typical use of the new _devicefinder_ node can be found in the _examples_ directory. To use it requires that the _node-red-dashboard_ has also been installed using the Palette Manager. The example flow may then be loaded using the Node-RED main menu's _Import_ item; in the window that pops up, select the _Examples_ tab and then navigate to _flows/node-red-contrib-devicefinder_ and select _ShowDevices_ and press the _Import_ button. This should open a new _Show Devices_ tab in the flow editor.

When deployed, connected X-keys devices are listed on your Node-RED's dashboard page (typically the same url as the flow editor page but adding _/ui_ to the url e.g. http://localhost:1880/ui (or, more generally, [http://\<ip-address\>:1880/ui]()). As devices are plugged into or removed from the host machine, their device name should appear in, and disappear from, the upper **DEVICE LIST** section of the dashboard display. Selecting any one of the listed devices produces information about that device in the middle **DEVICE INFO** section. If the **Read data** button in pressed and a device has been selected, the button text will change to **Reading ...**, after which any physical manipulation of the selected device (button press, jog, shuttle, joystick or T-bar movement) should produce corresponding data in the bottom data section. Pressing the **Clear** button clears the data section.

Double clicking a device, as well as selecting it, will blink the selected device's physical LED for a few seconds, thus confirming two way communication with the device.

A selected device may be soft rebooted using the **Reboot** button i.e. the device will restart as if it were physicially removed and then reattached to the host machine. In so doing, it will momentarily disappear from the dashboard's device list and reappear shortly afterward (within seconds). If no device is currently selected, the **Reboot** button has no effect.


## Upgrading

When a new version of _node-red-contrib-devicefinder_ is released, it will be visible in the Palette Manager after searching for _devicefinder_ in the Nodes tab. Pressing the _update to x.x.x_ button there will raise a notification warning of the need to restart Node-RED after the update occurs. Restart Node-RED by running `node-red-restart` in a terminal window. If using the example flow that is part of a _node-red-contrib-devicefinder_ release, it's a good idea to also update that too as it will typically have new functionality to showcase features of the update. Before updating, remove any flow tabs containing the old example flow and create a new flow with very little in it e.g. just an _Inject_ node. Now deploy this dummy flow to ensure that Node_RED doesn't try to deploy an old devicefinder example flow after the compulsory restart. After restart, load the updated example flow with the same procedure outlined in the Usage section above. After pressing the _Import_ button a warning about already existing nodes will appear; press the _Import copy_ button to complete the update. Then press Node-RED's _Deploy_ button to restart the new flow.

# Issues

On Linux, including Raspberry Pi, if no connected devices are detected, it's likely that there is a permissions issue which, as mentioned at [SuperFlyTV's xkeys project site](https://github.com/SuperFlyTV/xkeys), can be fixed by installing the appropriate configuration file as quoted here:

>Save the following to `/etc/udev/rules.d/50-xkeys.rules` and reload the rules with `sudo udevadm control --reload-rules && sudo udevadm trigger`
>
>```
>SUBSYSTEM=="input", GROUP="input", MODE="0666"
>SUBSYSTEM=="usb", ATTRS{idVendor}=="05f3", MODE:="0666", GROUP="plugdev"
>KERNEL=="hidraw*", ATTRS{idVendor}=="05f3", MODE="0666", GROUP="plugdev"
>```


## Contributing

Any bug reports and ideas for additional features are welcome via the [Issues section](https://gitlab.com/chris.willing/node-red-contrib-devicefinder/-/issues), as are code contributions via the Merge requests section.


## License
MIT
